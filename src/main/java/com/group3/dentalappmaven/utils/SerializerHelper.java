package com.group3.dentalappmaven.utils;

import com.group3.dentalappmaven.controller.CubicCurveManipulator;
import com.group3.dentalappmaven.controller.FXMLvisorController;
import com.group3.dentalappmaven.datamodel.*;
import com.group3.dentalappmaven.datamodel.Line;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;

import javax.imageio.ImageIO;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javafx.collections.ObservableList;

/**
 * Created by noviodelangel on 2016-10-20.
 */
public class SerializerHelper {

    private final String PAINT_HOME = "src\\main\\resources\\com\\group3\\dentalappmaven\\images\\paint\\";
    private Objects objects;

    public void prepareDataModel(Group pointPane, Group teethPane, Group outlinePane) {
        this.objects = new Objects();
        for (Node node : pointPane.getChildren()) {
            Circle circle = (Circle) node;
            this.objects.addPoint(new Point(circle));
        }

        for (Node node : teethPane.getChildren()) {
            ImageView imageView = (ImageView) node;
            this.objects.addTooth(new Tooth(imageView));
        }

        Outline outline = new Outline();
        for (Node node : outlinePane.getChildren()) {
            if (node.getClass().getName().equals(Circle.class.getName())) {
                Circle circle = (Circle) node;
                outline.addPoint(new Point(circle));
            } else if (node.getClass().getName().equals(javafx.scene.shape.Line.class.getName())) {
                javafx.scene.shape.Line line = (javafx.scene.shape.Line) node;
                outline.addLine(new Line(line));
            } else if (node.getClass().getName().equals(javafx.scene.shape.CubicCurve.class.getName())) {
                javafx.scene.shape.CubicCurve curve = (javafx.scene.shape.CubicCurve) node;
                
                outline.addCubicCurve(new Curve(curve));
            }
        }

        objects.setOutline(outline);
    }

    public void save(File file) {
        // save to file
        JAXBContext context = null;
        try {
            context = JAXBContext.newInstance(ObjectFactory.class.getPackage().getName());
            Marshaller marshaller = context.createMarshaller();
            marshaller.marshal(this.objects, file);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public void load(File file) {
        // load from file
        JAXBContext context = null;
        try {
            context = JAXBContext.newInstance(ObjectFactory.class.getPackage().getName());
            Unmarshaller unmarshaller = context.createUnmarshaller();
            this.objects = (Objects) unmarshaller.unmarshal(file);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public Integer restoreDataModel(Group pointPane, Group teethPane, Group outlinePane) {
        if (this.objects != null) {
            // restore points
            this.restorePoints(pointPane, objects.getPoints());

            // restore teeth
            this.restoreTeeth(teethPane, objects.getTeeth());

            // restore outline
            return this.restoreOutline(outlinePane, objects.getOutline());
        }
        return null;
    }

    public void restorePoints(Group pointPane, List<Point> points) {
        pointPane.getChildren().clear();
        for (Point point : points) {
            Circle circle = new Circle(point.getCenterX(), point.getCenterY(), point.getRadius(), Color.YELLOW);
            circle.setCursor(Cursor.MOVE);
            pointPane.getChildren().add(circle);
        }
    }

    public void restoreTeeth(Group teethPane, List<Tooth> teeth) {
        teethPane.getChildren().clear();
        for (Tooth tooth : teeth) {
            ImageView imageView;
            File f = new File(this.PAINT_HOME + tooth.getFilename());
            BufferedImage bufferedImage;
            try {
                bufferedImage = ImageIO.read(f);
                Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                imageView = new ImageView(image);
                imageView.setId(tooth.getFilename());
                imageView.setCursor(Cursor.MOVE);
                imageView.setX(tooth.getPosX());
                imageView.setY(tooth.getPosY());
                imageView.setRotate(tooth.getRotation());
                teethPane.getChildren().add(imageView);
            } catch (IOException ex) {
                System.out.println(ex.getMessage() + " " + f.getAbsolutePath());
            }
        }
    }

    public int restoreOutline(Group outlinePane, Outline outline) { // returns how many points for outline exist
        outlinePane.getChildren().clear();
        for (Point point : outline.getPoints()) {
            Circle circle = new Circle(point.getCenterX(), point.getCenterY(), point.getRadius());
            circle.setCursor(Cursor.MOVE);
            circle.setFill(Color.DEEPPINK.deriveColor(1, 1, 1, 0.5));
            circle.setStroke(Color.DEEPPINK);
            circle.setStrokeWidth(2);
            circle.setStrokeType(StrokeType.OUTSIDE);
            outlinePane.getChildren().add(circle);
        }
        ObservableList<Node> iPoint = outlinePane.getChildren().filtered(p -> p.getClass() == javafx.scene.shape.Circle.class);
        int nPoint = iPoint.size();
        List<Curve> curves=outline.getCurves();
        for (int i = 0; i < nPoint - 1; i++) {
            Circle c1 = (Circle) iPoint.get(i);
            Circle c2 = (Circle) iPoint.get(i + 1);
            Curve c=curves.get(i);
            CubicCurveManipulator aux = new CubicCurveManipulator(c1, c2,c.getControlX1(),c.getControlY1(),c.getControlX2(),c.getControlY2());
            outlinePane.getChildren().add(aux.getControlLine1());
            outlinePane.getChildren().add(aux.getControlLine2());
            outlinePane.getChildren().add(aux.getControl1());
            outlinePane.getChildren().add(aux.getControl2());
            outlinePane.getChildren().add(aux.getCurve());
        }
        return outlinePane.getChildren().filtered(p -> p.getClass() == javafx.scene.shape.Circle.class).size();

    }

    public File unzipImageFile(FileHeader imageFileHeader, ZipFile zipFile) throws ZipException {
        zipFile.extractFile(imageFileHeader, FXMLvisorController.ZIP_TEMP_FOLDER);
        return new File(FXMLvisorController.ZIP_TEMP_FOLDER + FXMLvisorController.IMAGE_NAME);
    }

    public File unzipXmlFile(FileHeader xmlFileHeader, ZipFile zipFile) throws ZipException {
        zipFile.extractFile(xmlFileHeader, FXMLvisorController.ZIP_TEMP_FOLDER);
        return new File(FXMLvisorController.ZIP_TEMP_FOLDER + FXMLvisorController.XML_NAME);
    }

}
