package com.group3.dentalappmaven.datamodel;

import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by noviodelangel on 2016-10-20.
 */
@XmlType(name = "point")
@XmlAccessorType(XmlAccessType.FIELD)
public class Point {

	private double centerX;
	private double centerY;
	private double radius;

	public Point() {
	}

	public Point(Circle c){
		this.centerX = c.getCenterX();
		this.centerY = c.getCenterY();
		this.radius = c.getRadius();
	}

	public double getCenterX() {
		return centerX;
	}

	public void setCenterX(double centerX) {
		this.centerX = centerX;
	}

	public double getCenterY() {
		return centerY;
	}

	public void setCenterY(double centerY) {
		this.centerY = centerY;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}
}
