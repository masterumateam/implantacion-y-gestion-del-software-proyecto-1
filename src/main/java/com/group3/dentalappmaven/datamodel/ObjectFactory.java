package com.group3.dentalappmaven.datamodel;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * Created by noviodelangel on 2016-10-23.
 */
@XmlRegistry
public class ObjectFactory {
	public Objects createObjects() {
		return new Objects();
	}

	public Point createPoint() {
		return new Point();
	}

	public Tooth createTooth() {
		return new Tooth();
	}

	public Line createLine() {
		return new Line();
	}

	public Outline createOutline() {
		return new Outline();
	}
        
        public Curve createCurve(){
            return new Curve();
        }

}
