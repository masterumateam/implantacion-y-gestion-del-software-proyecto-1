package com.group3.dentalappmaven.datamodel;

import javafx.scene.image.ImageView;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by noviodelangel on 2016-10-20.
 */
@XmlType(name = "tooth")
@XmlAccessorType(XmlAccessType.FIELD)
public class Tooth {

	private double posX;
	private double posY;
	private double rotation;
	private String filename;

	public Tooth() {
	}

	public Tooth(ImageView iv) {
		this.posX = iv.getX();
		this.posY = iv.getY();
		this.filename = iv.getId();
		this.rotation = iv.getRotate();
	}

	public double getPosX() {
		return posX;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosY() {
		return posY;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public double getRotation() {
		return rotation;
	}

	public void setRotation(double rotation) {
		this.rotation = rotation;
	}
}
