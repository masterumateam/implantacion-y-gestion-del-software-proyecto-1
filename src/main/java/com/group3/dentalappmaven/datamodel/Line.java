package com.group3.dentalappmaven.datamodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by noviodelangel on 2016-10-23.
 */
@XmlType(name = "line")
@XmlAccessorType(XmlAccessType.FIELD)
public class Line {
	private double startX;
	private double startY;
	private double endX;
	private double endY;

	public Line() {
	}

	public Line(javafx.scene.shape.Line line) {
		this.startX = line.getStartX();
		this.startY = line.getStartY();
		this.endX = line.getEndX();
		this.endY = line.getEndY();
	}

	public double getStartX() {
		return startX;
	}

	public void setStartX(double startX) {
		this.startX = startX;
	}

	public double getStartY() {
		return startY;
	}

	public void setStartY(double startY) {
		this.startY = startY;
	}

	public double getEndX() {
		return endX;
	}

	public void setEndX(double endX) {
		this.endX = endX;
	}

	public double getEndY() {
		return endY;
	}

	public void setEndY(double endY) {
		this.endY = endY;
	}
}
