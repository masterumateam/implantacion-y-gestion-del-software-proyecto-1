package com.group3.dentalappmaven.datamodel;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by noviodelangel on 2016-10-20.
 */
@XmlRootElement(name = "objects")
@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class Objects {

	@XmlElement(name = "point")
	private List<Point> points = new ArrayList<>();

	@XmlElement(name = "tooth")
	private List<Tooth> teeth = new ArrayList<>();

	@XmlElement(name = "outline")
	private Outline outline;

	public List<Point> getPoints() {
		return points;
	}

	public void setPoints(List<Point> points) {
		this.points = points;
	}

	public List<Tooth> getTeeth() {
		return teeth;
	}

	public void setTeeth(List<Tooth> teeth) {
		this.teeth = teeth;
	}

	public Outline getOutline() {
		return outline;
	}

	public void setOutline(Outline outline) {
		this.outline = outline;
	}

	public void addPoint(Point point) {
		if (this.points == null) {
			this.points = new ArrayList<>();
		}
		points.add(point);
	}

	public void addTooth(Tooth tooth) {
		if (this.teeth == null) {
			this.teeth = new ArrayList<>();
		}
		teeth.add(tooth);
	}
}
