package com.group3.dentalappmaven.datamodel;

import javafx.scene.paint.Paint;
import javafx.scene.shape.*;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by noviodelangel on 2016-10-20.
 */
@XmlType(name = "curve")
@XmlAccessorType(XmlAccessType.FIELD)
public class Curve {

    private double startX;
    private double startY;
    private double endY;
    private double endX;

    private double controlX1;
    private double controlX2;
    private double controlY1;
    private double controlY2;

    public Curve() {
    }

    public Curve(CubicCurve c) {
        this.controlX1 = c.getControlX1();
        this.controlX2 = c.getControlX2();
        this.controlY1 = c.getControlY1();
        this.controlY2 = c.getControlY2();

        this.startX = c.getStartX();
        this.startY = c.getStartY();
        this.endX = c.getEndX();
        this.endY = c.getEndY();
    }

    public double getStartX() {
        return startX;
    }

    public void setStartX(double startX) {
        this.startX = startX;
    }

    public double getStartY() {
        return startY;
    }

    public void setStartY(double startY) {
        this.startY = startY;
    }

    public double getEndY() {
        return endY;
    }

    public void setEndY(double endY) {
        this.endY = endY;
    }

    public double getEndX() {
        return endX;
    }

    public void setEndX(double endX) {
        this.endX = endX;
    }

    public double getControlX1() {
        return controlX1;
    }

    public void setControlX1(double controlX1) {
        this.controlX1 = controlX1;
    }

    public double getControlX2() {
        return controlX2;
    }

    public void setControlX2(double controlX2) {
        this.controlX2 = controlX2;
    }

    public double getControlY1() {
        return controlY1;
    }

    public void setControlY1(double controlY1) {
        this.controlY1 = controlY1;
    }

    public double getControlY2() {
        return controlY2;
    }

    public void setControlY2(double controlY2) {
        this.controlY2 = controlY2;
    }

}
