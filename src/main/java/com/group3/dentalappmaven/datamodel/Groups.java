package com.group3.dentalappmaven.datamodel;

import javafx.scene.Group;

/**
 * Created by noviodelangel on 2016-10-24.
 */
public class Groups {
	private Group pointPane;
	private Group teethPane;
	private Group outlinePane;

	public Group getPointPane() {
		return pointPane;
	}

	public void setPointPane(Group pointPane) {
		this.pointPane = pointPane;
	}

	public Group getTeethPane() {
		return teethPane;
	}

	public void setTeethPane(Group teethPane) {
		this.teethPane = teethPane;
	}

	public Group getOutlinePane() {
		return outlinePane;
	}

	public void setOutlinePane(Group outlinePane) {
		this.outlinePane = outlinePane;
	}
}
