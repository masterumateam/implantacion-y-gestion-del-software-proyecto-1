package com.group3.dentalappmaven.datamodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.shape.CubicCurve;

/**
 * Created by noviodelangel on 2016-10-20.
 */
@XmlType(name = "outline")
@XmlAccessorType(XmlAccessType.FIELD)
public class Outline {

    @XmlElement(name = "point")
    private List<Point> points = new ArrayList<>();

    @XmlElement(name = "line")
    private List<Line> lines = new ArrayList<>();

    @XmlElement(name = "curve")
    private List<Curve> curves = new ArrayList<>();

    public void addPoint(Point point) {
        if (this.points == null) {
            this.points = new ArrayList<>();
        }
        points.add(point);
    }

    public void addLine(Line line) {
        if (this.lines == null) {
            this.lines = new ArrayList<>();
        }
        lines.add(line);
    }

    public void addCubicCurve(Curve curve) {
        if (this.curves == null) {
            this.curves = new ArrayList<>();
        }
        curves.add(curve);
    }

    public List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }

    public List<Line> getLines() {
        return lines;
    }

    public void setLines(List<Line> lines) {
        this.lines = lines;
    }

    public List<Curve> getCurves() {
        return curves;
    }

    public void setCurves(List<Curve> curves) {
        this.curves = curves;
    }

}
