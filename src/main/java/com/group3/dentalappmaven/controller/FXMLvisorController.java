package com.group3.dentalappmaven.controller;

import com.group3.dentalappmaven.controller.CubicCurveManipulator.Anchor;
import static com.group3.dentalappmaven.controller.CubicCurveManipulator.changePoint;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.group3.dentalappmaven.utils.SerializerHelper;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.CubicCurve;
import javafx.scene.shape.Line;
import javafx.scene.shape.StrokeType;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

import javax.imageio.ImageIO;

/**
 * FXML Controller class
 *
 * @author GRUPO 3
 */
public class FXMLvisorController implements Initializable {

    /**
     * Initializes the controller class.
     */
    //MENU
    @FXML
    MenuItem closeFileMenu;
    @FXML
    MenuItem openFileMenu;
    @FXML
    MenuItem saveAsMenu;
    @FXML
    MenuItem openProjectMenu;
    @FXML
    MenuItem saveProjectMenu;

    //PANELS
    @FXML
    StackPane toolsPane;
    @FXML
    AnchorPane ap;
    @FXML
    ImageView visor;
    @FXML
    ImageView visorNoImage;
    @FXML
    StackPane viewerPane;
    @FXML
    ScrollPane s1;
    @FXML
    Group allPane;
    //BUTTONS
    @FXML
    ToggleButton addPointBt;
    @FXML
    Button resetZoomBt;
    @FXML
    ToggleButton createOutlineBt;
    @FXML
    Button rebuildOutlineBt;
    @FXML
    ToggleButton distance;
    @FXML
    ToggleButton angle;
    @FXML
    ToggleButton addMolarDown;
    @FXML
    ToggleButton addMolarUp;
    @FXML
    ToggleButton addIncisorDown;
    @FXML
    ToggleButton addIncisorUp;
    
    //MESSAGE
    @FXML
    Label lbStatus;
    @FXML
    Label nPointOutlineLb;

    //LAYERS
    @FXML
    Group pointPane;
    @FXML
    CheckBox seePointsCB;
    @FXML
    Group teethPane;
    @FXML
    CheckBox seeTeethCB;
    @FXML
    Group outlinePane;
    @FXML
    CheckBox seeOutlineCB;
    @FXML
    CheckBox seeControlsCB;

    @FXML
    Label distanceLb;
    @FXML
    Label angleLb;

    List<Circle> mediciones = new ArrayList<>();
    ImageView selectedImg = null;
    double imagev; //VERTICAL HEIGHT
    double imageh; //HORIZONTAL WIDTH
    double defaulth; //DEFAULT ZOOM H
    double defaultv; //DEFAULT ZOOM W
    double orgSceneX, orgSceneY;
    double orgTranslateX, orgTranslateY;
    ToggleGroup groupTeeth;
    ToggleGroup groupPoint;
    ToggleGroup groupMeasures;
    int nPoint = 14;
    double factor = 1.25;
    int zoom = 0;
    boolean first = true;
    String name;
    private BufferedImage currentOpenImage;
    public static final String ZIP_TEMP_FOLDER = "src\\main\\resources\\com\\group3\\dentalappmaven\\zipTemp\\"; // for storing partial files for zip package
    public static final String XML_NAME = "objects.xml";
    public static final String IMAGE_NAME = "image.jpg";

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        File f = new File("src\\main\\resources\\com\\group3\\dentalappmaven\\images\\noimage.png");
        BufferedImage bufferedImage;
        try {
            bufferedImage = ImageIO.read(f);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            visorNoImage.setImage(image);
            visorNoImage.setDisable(true);

        } catch (IOException ex) {
            System.out.println(ex.getMessage() + " " + f.getAbsolutePath());
        }
        lbStatus.setText("Open File...");
        visor.setFitHeight(s1.getHeight());
        visor.setFitWidth(s1.getWidth());
        visor.setVisible(false);
        s1.setStyle("-fx-font-size: 20px;");
        toolsPane.setDisable(true);
        groupPoint = new ToggleGroup();
        groupMeasures = new ToggleGroup();
        groupTeeth = new ToggleGroup();
        addPointBt.setToggleGroup(groupPoint);
        createOutlineBt.setToggleGroup(groupPoint);
        distance.setToggleGroup(groupMeasures);
        angle.setToggleGroup(groupMeasures);

        addIncisorDown.setToggleGroup(groupTeeth);
        addIncisorUp.setToggleGroup(groupTeeth);
        addMolarDown.setToggleGroup(groupTeeth);
        addMolarUp.setToggleGroup(groupTeeth);

    }

    //MENU
    @FXML
    public void openPicture(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Picture File");
        FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("Pictures files (*.jpg, *.png)", "*.JPG", "*.PNG");
        fileChooser.getExtensionFilters().addAll(extFilterJPG);
        File f = fileChooser.showOpenDialog(null);
        if (f != null) {
            try {
                currentOpenImage = ImageIO.read(f);
                Image image = SwingFXUtils.toFXImage(currentOpenImage, null);
                visor.setPreserveRatio(true);
                visor.setImage(image);
                if (image.getHeight() >= s1.getHeight() || image.getWidth() >= s1.getWidth()) {
                    imagev = viewerPane.getHeight();
                    imageh = viewerPane.getWidth();
                    System.out.println("1");
                } else {
                    imagev = image.getHeight();
                    imageh = image.getWidth();
                    System.out.println("factor");
                }

                visor.setFitHeight(imagev);
                visor.setFitWidth(imageh);
                defaulth = imageh;
                defaultv = imagev;
                lbStatus.setText("File " + f.getName());
                visorNoImage.setVisible(false);
                visor.setVisible(true);
                Stage stage = (Stage) ap.getScene().getWindow();
                stage.setTitle("DentalApp - " + f.getName());
                openFileMenu.setDisable(true);
                closeFileMenu.setDisable(false);
                saveAsMenu.setDisable(false);
                saveProjectMenu.setDisable(false);
                toolsPane.setDisable(false);
                nPointOutlineLb.setText("" + nPoint);
                nPointOutlineLb.setTextFill(Color.RED);
                name = f.getName();
            } catch (IOException ex) {
                lbStatus.setText("Error open file " + f.getName());
            }
        }
    }

    @FXML
    public void closePicture(ActionEvent event) {
        Alert alert = new Alert(AlertType.CONFIRMATION, "Do you want to close this project " + name + " ?", ButtonType.YES, ButtonType.CANCEL);
        alert.showAndWait();
        if (alert.getResult() == ButtonType.YES) {
            visor.setVisible(false);
            visorNoImage.setVisible(true);
            openFileMenu.setDisable(false);
            closeFileMenu.setDisable(true);
            saveAsMenu.setDisable(true);
            pointPane.getChildren().clear();
            teethPane.getChildren().clear();
            outlinePane.getChildren().clear();
            toolsPane.setDisable(true);
            nPointOutlineLb.setText("" + nPoint);
            nPointOutlineLb.setTextFill(Color.RED);
            createOutlineBt.setVisible(true);
            rebuildOutlineBt.setVisible(false);
            saveProjectMenu.setDisable(true);
            currentOpenImage = null;
        }

    }

    @FXML
    public void saveAs(ActionEvent event) {
        boolean seeControlsBefore = seeControlsCB.isSelected();
        FileChooser fileChooser = new FileChooser();

        //Set extension filter
        FileChooser.ExtensionFilter extFilter
                = new FileChooser.ExtensionFilter("PNG files (*.PNG)", "*.png");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File file = fileChooser.showSaveDialog(null);

        if (file != null) {
            try {
                zoomDefault();
                WritableImage writableImage = new WritableImage((int) visor.getFitWidth(), (int) visor.getFitHeight());
                if (seeControlsBefore) {
                    seeControlsCB.setSelected(false);
                }
                seeControls();
                allPane.snapshot(null, writableImage);
                RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);
                ImageIO.write(renderedImage, "png", file);

                if (seeControlsBefore) {
                    seeControlsCB.setSelected(true);
                }
                seeControls();
            } catch (IOException ex) {
                lbStatus.setText("ERROR: Can't save file");
            }
        }
    }

    @FXML
    public void openProject(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("zip files (*.zip)", "*.zip");
        fileChooser.getExtensionFilters().add(extensionFilter);
        fileChooser.setSelectedExtensionFilter(extensionFilter);

        File file = fileChooser.showOpenDialog(null);
        if (file != null) {
            // Decompress zip
            ZipFile zipFile;
            File imageFile = null;
            File xmlFile = null;
            try {
                zipFile = new ZipFile(file);
                @SuppressWarnings("unchecked")
                List<FileHeader> fileHeaders = zipFile.getFileHeaders();
                if (fileHeaders.size() != 2) {
                    new Alert(Alert.AlertType.ERROR, "Zip structure is incorrect!\nThere should be exactly two files inside.", ButtonType.OK).showAndWait();
                    return;
                }

                SerializerHelper serializerHelper = new SerializerHelper();
                if (fileHeaders.get(0).getFileName().endsWith(".jpg")) {
                    imageFile = serializerHelper.unzipImageFile(fileHeaders.get(0), zipFile);
                } else if (fileHeaders.get(0).getFileName().endsWith(".xml")) {
                    xmlFile = serializerHelper.unzipXmlFile(fileHeaders.get(0), zipFile);
                }

                if (fileHeaders.get(1).getFileName().endsWith(".jpg")) {
                    imageFile = serializerHelper.unzipImageFile(fileHeaders.get(1), zipFile);
                } else if (fileHeaders.get(1).getFileName().endsWith(".xml")) {
                    xmlFile = serializerHelper.unzipXmlFile(fileHeaders.get(1), zipFile);
                }

            } catch (ZipException e) {
                e.printStackTrace();
                new Alert(Alert.AlertType.ERROR, "Could not decompress the zip!\n" + e.getMessage(), ButtonType.OK).showAndWait();
                return;
            }

            // Load image
            try {
                currentOpenImage = ImageIO.read(imageFile);
                Image image = SwingFXUtils.toFXImage(currentOpenImage, null);
                visor.setPreserveRatio(true);
                visor.setImage(image);
                if (image.getHeight() >= s1.getHeight() || image.getWidth() >= s1.getWidth()) {
                    imagev = viewerPane.getHeight();
                    imageh = viewerPane.getWidth();
                    System.out.println("1");
                } else {
                    imagev = image.getHeight();
                    imageh = image.getWidth();
                    System.out.println("factor");
                }
                visor.setFitHeight(imagev);
                visor.setFitWidth(imageh);
                defaulth = imageh;
                defaultv = imagev;
                lbStatus.setText("File " + imageFile.getName());
                visorNoImage.setVisible(false);
                visor.setVisible(true);
                Stage stage = (Stage) ap.getScene().getWindow();
                stage.setTitle("DentalApp - " + imageFile.getName());
                openFileMenu.setDisable(true);
                closeFileMenu.setDisable(false);
                saveAsMenu.setDisable(false);
                saveProjectMenu.setDisable(false);
                toolsPane.setDisable(false);
                name = imageFile.getName();
            } catch (IOException ex) {
                ex.printStackTrace();
                new Alert(Alert.AlertType.ERROR, "Error open file " + imageFile.getName(), ButtonType.OK).showAndWait();
            }

            // Load xml
            SerializerHelper helper = new SerializerHelper();
            helper.load(xmlFile);
            Integer outlinePointsCount = helper.restoreDataModel(pointPane, teethPane, outlinePane);
            int outlinePointsLeft = ((outlinePointsCount == null) ? nPoint : nPoint - outlinePointsCount);
            nPointOutlineLb.setText("" + outlinePointsLeft);
            if (outlinePointsLeft > 0) {
                nPointOutlineLb.setTextFill(Color.RED);
                createOutlineBt.setVisible(true);
                rebuildOutlineBt.setVisible(false);
            } else if (outlinePointsLeft == 0) {
                toolsPane.setDisable(false);
                createOutlineBt.setSelected(false);
                createOutlineBt.setVisible(false);
                rebuildOutlineBt.setVisible(true);
                nPointOutlineLb.setTextFill(Color.GREEN);

            }
            restoreEventsHandlers();

            // Delete extracted files
            xmlFile.delete();
            imageFile.delete();
        }
    }

    @FXML
    public void saveProject(ActionEvent event) {
        if (this.currentOpenImage != null) {
            FileChooser fileChooser = new FileChooser();
            FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("zip files (*.zip)", "*.zip");
            fileChooser.getExtensionFilters().add(extensionFilter);
            fileChooser.setSelectedExtensionFilter(extensionFilter);

            File file = fileChooser.showSaveDialog(null);
            if (file != null) {
                // prepare xml
                SerializerHelper helper = new SerializerHelper();
                helper.prepareDataModel(pointPane, teethPane, outlinePane);
                File xmlFile = new File(ZIP_TEMP_FOLDER + XML_NAME);
                helper.save(xmlFile);

                // prepare image
                File imageFile = new File(ZIP_TEMP_FOLDER + IMAGE_NAME);
                try {
                    ImageIO.write(this.currentOpenImage, "jpg", imageFile);
                } catch (IOException e) {
                    e.printStackTrace();
                    new Alert(Alert.AlertType.ERROR, "Could not save image!\n" + e.getMessage(), ButtonType.OK).showAndWait();
                    return;
                }

                // make zip
                ZipFile zipFile = null;
                try {
                    zipFile = new ZipFile(file);
                } catch (ZipException e) {
                    e.printStackTrace();
                    new Alert(Alert.AlertType.ERROR, "Could not create the zip file!\n" + e.getMessage(), ButtonType.OK).showAndWait();
                    return;
                }
                ZipParameters zipParameters = new ZipParameters();
                zipParameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
                zipParameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
                try {
                    zipFile.addFile(xmlFile, zipParameters);
                    zipFile.addFile(imageFile, zipParameters);
                } catch (ZipException e) {
                    e.printStackTrace();
                    new Alert(Alert.AlertType.ERROR, "Could not create the zip file!\n" + e.getMessage(), ButtonType.OK).showAndWait();
                    return;
                }
                // delete temp files
                xmlFile.delete();
                imageFile.delete();
            }
        }
    }

    private void restoreEventsHandlers() {
        for (Node node : this.pointPane.getChildren()) {
            Circle circle = (Circle) node;
            circle.setOnMousePressed(circleOnMousePressedEventHandler);
            circle.setOnMouseDragged(circleOnMouseDraggedEventHandler);
            circle.setOnMouseClicked(circleOnMouseRightClickEventHandler);
        }

        for (Node node : this.teethPane.getChildren()) {
            ImageView imageView = (ImageView) node;
            imageView.setOnMousePressed(imageOnMousePressedEventHandler);
            imageView.setOnMouseDragged(imageOnMouseDraggedEventHandler);
            imageView.setOnMouseClicked(imageOnMouseRightClickEventHandler);
        }

        for (Node node : this.outlinePane.getChildren()) {
            if (node.getClass().getName().equals(Circle.class.getName())) {
                Circle circle = (Circle) node;
                circle.setOnMousePressed(circleOnMousePressedEventHandler);
                circle.setOnMouseDragged(circleOutlineOnMouseDraggedEventHandler);
                circle.setOnMouseClicked(circleOutlineOnMouseRightClickEventHandler);
            }
        }
    }

    @FXML
    public void closeWindow(ActionEvent event) {
        Alert alert = new Alert(AlertType.CONFIRMATION, "Do you want to exit ?", ButtonType.YES, ButtonType.CANCEL);
        alert.showAndWait();
        if (alert.getResult() == ButtonType.YES) {
            Platform.exit();
        }
    }

    //ZOOM
    @FXML
    public void zoomPlus() {
        imagev = imagev * factor;
        imageh = imageh * factor;
        visor.setFitWidth(imageh);
        visor.setFitHeight(imagev);
        movePointsPlus();
        moveTeethPlus();
        movePointsOutlinePlus();
        moveLineOutlinePlus();
        zoom++;
        if (zoom == 0) {
            resetZoomBt.setDisable(true);
        } else {
            resetZoomBt.setDisable(false);
        }
        System.out.println("VISOR: " + visor.getFitWidth() + ":" + visor.getFitHeight());
    }

    @FXML
    public void zoomMinus() {
        imagev = imagev / factor;
        imageh = imageh / factor;
        visor.setFitWidth(imageh);
        visor.setFitHeight(imagev);
        movePointsMinus();
        moveTeethMinus();
        movePointsOutlineMinus();
        moveLineOutlineMinus();
        zoom--;
        if (zoom == 0) {
            resetZoomBt.setDisable(true);
        } else {
            resetZoomBt.setDisable(false);
        }
        System.out.println("VISOR: " + visor.getFitWidth() + ":" + visor.getFitHeight());
    }

    @FXML
    public void zoomDefault() {
        visor.setFitWidth(defaulth);
        visor.setFitHeight(defaultv);
        imagev = defaultv;
        imageh = defaulth;
        System.out.println("ZOOM" + zoom);
        movePointsDefault(zoom);
        moveTeethDefault(zoom);
        movePointsOutlineDefault(zoom);
        moveLineOutlineDefault(zoom);
        zoom = 0;
        resetZoomBt.setDisable(true);
        System.out.println("VISOR: " + visor.getFitWidth() + ":" + visor.getFitHeight());
    }

    //LAYERS
    @FXML
    public void seePoints() {
        if (seePointsCB.isSelected()) {
            pointPane.setVisible(true);
        } else {
            pointPane.setVisible(false);
        }
    }

    @FXML
    public void seeTeeth() {
        if (seeTeethCB.isSelected()) {
            teethPane.setVisible(true);
        } else {
            teethPane.setVisible(false);
        }
    }

    @FXML
    public void seeOutline() {
        if (seeOutlineCB.isSelected()) {
            outlinePane.setVisible(true);
        } else {
            outlinePane.setVisible(false);
        }

    }

    @FXML
    public void seeControls() {
        if (seeControlsCB.isSelected()) {
            
            for (Node i : outlinePane.getChildren()) {
                if (i.getClass() != Circle.class && i.getClass() != CubicCurve.class) {
                    i.setVisible(true);
                }
            }
        } else {
            
            for (Node i : outlinePane.getChildren()) {
                if (i.getClass() != Circle.class && i.getClass() != CubicCurve.class) {
                    i.setVisible(false);
                }
            }
        }
    }

    /*@FXML
    public void doZoom(MouseEvent event) {
        if (event.getButton() == MouseButton.SECONDARY) {
            zoomMinus();
        } else if (event.getButton() == MouseButton.PRIMARY) {
            zoomPlus();
        } else if (event.getButton() == MouseButton.MIDDLE) {
            zoomDefault();
        }
    }*/
    private void movePointsPlus() {
        for (Node aux : pointPane.getChildren()) {
            ((Circle) aux).setCenterX(((Circle) aux).getCenterX() * factor);
            ((Circle) aux).setCenterY(((Circle) aux).getCenterY() * factor);
        }
    }

    private void movePointsOutlinePlus() {
        for (Node aux : outlinePane.getChildren().filtered(p -> p.getClass() == Circle.class)) {
            ((Circle) aux).setCenterX(((Circle) aux).getCenterX() * factor);
            ((Circle) aux).setCenterY(((Circle) aux).getCenterY() * factor);
        }
    }

    private void moveLineOutlinePlus() {
        for (Node aux : outlinePane.getChildren().filtered(p -> p.getClass() == Line.class)) {

            ((Line) aux).setStartX(((Line) aux).getStartX() * factor);
            ((Line) aux).setStartY(((Line) aux).getStartY() * factor);
            ((Line) aux).setEndX(((Line) aux).getEndX() * factor);
            ((Line) aux).setEndY(((Line) aux).getEndY() * factor);

        }
        for (Node aux : outlinePane.getChildren().filtered(p -> p.getClass() == CubicCurve.class)) {

            ((CubicCurve) aux).setStartX(((CubicCurve) aux).getStartX() * factor);
            ((CubicCurve) aux).setStartY(((CubicCurve) aux).getStartY() * factor);
            ((CubicCurve) aux).setEndX(((CubicCurve) aux).getEndX() * factor);
            ((CubicCurve) aux).setEndY(((CubicCurve) aux).getEndY() * factor);
        }
        for (Node aux : outlinePane.getChildren().filtered(p -> p.getClass() == Anchor.class)) {
            ((Anchor) aux).setCenterX(((Anchor) aux).getCenterX() * factor);
            ((Anchor) aux).setCenterY(((Anchor) aux).getCenterY() * factor);
        }
    }

    private void moveTeethPlus() {
        for (Node aux : teethPane.getChildren()) {

            ((ImageView) (aux)).setX((((ImageView) aux).getX() * factor));
            ((ImageView) (aux)).setY((((ImageView) aux).getY() * factor));
            ((ImageView) (aux)).setScaleX(((ImageView) (aux)).getScaleX() * factor);
            ((ImageView) (aux)).setScaleY(((ImageView) (aux)).getScaleY() * factor);
        }
    }

    private void movePointsMinus() {
        for (Node aux : pointPane.getChildren()) {
            ((Circle) aux).setCenterX(((Circle) aux).getCenterX() / factor);
            ((Circle) aux).setCenterY(((Circle) aux).getCenterY() / factor);
        }
    }

    private void moveTeethMinus() {
        for (Node aux : teethPane.getChildren()) {
            ((ImageView) (aux)).setScaleX(((ImageView) (aux)).getScaleX() / factor);
            ((ImageView) (aux)).setScaleY(((ImageView) (aux)).getScaleY() / factor);
            ((ImageView) (aux)).setX((((ImageView) aux).getX() / factor));
            ((ImageView) (aux)).setY((((ImageView) aux).getY() / factor));
        }
    }

    private void movePointsOutlineMinus() {
        for (Node aux : outlinePane.getChildren().filtered(p -> p.getClass() == Circle.class)) {
            ((Circle) aux).setCenterX(((Circle) aux).getCenterX() / factor);
            ((Circle) aux).setCenterY(((Circle) aux).getCenterY() / factor);
        }
    }

    private void moveLineOutlineMinus() {
        for (Node aux : outlinePane.getChildren().filtered(p -> p.getClass() == Line.class)) {

            ((Line) aux).setStartX(((Line) aux).getStartX() / factor);
            ((Line) aux).setStartY(((Line) aux).getStartY() / factor);
            ((Line) aux).setEndX(((Line) aux).getEndX() / factor);
            ((Line) aux).setEndY(((Line) aux).getEndY() / factor);

        }
        for (Node aux : outlinePane.getChildren().filtered(p -> p.getClass() == CubicCurve.class)) {
            ((CubicCurve) aux).setStartX(((CubicCurve) aux).getStartX() / factor);
            ((CubicCurve) aux).setStartY(((CubicCurve) aux).getStartY() / factor);
            ((CubicCurve) aux).setEndX(((CubicCurve) aux).getEndX() / factor);
            ((CubicCurve) aux).setEndY(((CubicCurve) aux).getEndY() / factor);
        }
        for (Node aux : outlinePane.getChildren().filtered(p -> p.getClass() == Anchor.class)) {
            ((Anchor) aux).setCenterX(((Anchor) aux).getCenterX() / factor);
            ((Anchor) aux).setCenterY(((Anchor) aux).getCenterY() / factor);
        }
    }

    private void movePointsDefault(int zoom) {
        if (zoom < 0) {
            for (Node aux : pointPane.getChildren()) {
                for (int i = zoom; i < 0; i++) {
                    ((Circle) aux).setCenterX(((Circle) aux).getCenterX() * factor);
                    ((Circle) aux).setCenterY(((Circle) aux).getCenterY() * factor);
                }
            }
        } else if (zoom > 0) {
            for (Node aux : pointPane.getChildren()) {
                for (int i = zoom; i > 0; i--) {
                    ((Circle) aux).setCenterX(((Circle) aux).getCenterX() / factor);
                    ((Circle) aux).setCenterY(((Circle) aux).getCenterY() / factor);
                }
            }
        }
    }

    private void movePointsOutlineDefault(int zoom) {
        if (zoom < 0) {
            for (Node aux : outlinePane.getChildren().filtered(p -> p.getClass() == Circle.class)) {
                for (int i = zoom; i < 0; i++) {
                    ((Circle) aux).setCenterX(((Circle) aux).getCenterX() * factor);
                    ((Circle) aux).setCenterY(((Circle) aux).getCenterY() * factor);
                }
            }
        } else if (zoom > 0) {
            for (Node aux : outlinePane.getChildren().filtered(p -> p.getClass() == Circle.class)) {
                for (int i = zoom; i > 0; i--) {
                    ((Circle) aux).setCenterX(((Circle) aux).getCenterX() / factor);
                    ((Circle) aux).setCenterY(((Circle) aux).getCenterY() / factor);
                }
            }
        }
    }

    private void moveLineOutlineDefault(int zoom) {
        if (zoom < 0) {
            for (Node aux : outlinePane.getChildren().filtered(p -> p.getClass() == Line.class)) {
                for (int i = zoom; i < 0; i++) {
                    ((Line) aux).setStartX(((Line) aux).getStartX() * factor);
                    ((Line) aux).setStartY(((Line) aux).getStartY() * factor);
                    ((Line) aux).setEndX(((Line) aux).getEndX() * factor);
                    ((Line) aux).setEndY(((Line) aux).getEndY() * factor);
                }
            }
            for (Node aux : outlinePane.getChildren().filtered(p -> p.getClass() == CubicCurve.class)) {
                for (int i = zoom; i < 0; i++) {
                    ((CubicCurve) aux).setStartX(((CubicCurve) aux).getStartX() * factor);
                    ((CubicCurve) aux).setStartY(((CubicCurve) aux).getStartY() * factor);
                    ((CubicCurve) aux).setEndX(((CubicCurve) aux).getEndX() * factor);
                    ((CubicCurve) aux).setEndY(((CubicCurve) aux).getEndY() * factor);
                }
            }
            for (Node aux : outlinePane.getChildren().filtered(p -> p.getClass() == Anchor.class)) {
                for (int i = zoom; i < 0; i++) {
                    ((Anchor) aux).setCenterX(((Anchor) aux).getCenterX() * factor);
                    ((Anchor) aux).setCenterY(((Anchor) aux).getCenterY() * factor);
                }
            }
        } else if (zoom > 0) {
            for (Node aux : outlinePane.getChildren().filtered(p -> p.getClass() == Line.class)) {
                for (int i = zoom; i > 0; i--) {
                    ((Line) aux).setStartX(((Line) aux).getStartX() / factor);
                    ((Line) aux).setStartY(((Line) aux).getStartY() / factor);
                    ((Line) aux).setEndX(((Line) aux).getEndX() / factor);
                    ((Line) aux).setEndY(((Line) aux).getEndY() / factor);
                }
            }
            for (Node aux : outlinePane.getChildren().filtered(p -> p.getClass() == CubicCurve.class)) {
                for (int i = zoom; i > 0; i--) {
                    ((CubicCurve) aux).setStartX(((CubicCurve) aux).getStartX() / factor);
                    ((CubicCurve) aux).setStartY(((CubicCurve) aux).getStartY() / factor);
                    ((CubicCurve) aux).setEndX(((CubicCurve) aux).getEndX() / factor);
                    ((CubicCurve) aux).setEndY(((CubicCurve) aux).getEndY() / factor);
                }
            }
            for (Node aux : outlinePane.getChildren().filtered(p -> p.getClass() == Anchor.class)) {
                for (int i = zoom; i > 0; i--) {
                    ((Anchor) aux).setCenterX(((Anchor) aux).getCenterX() / factor);
                    ((Anchor) aux).setCenterY(((Anchor) aux).getCenterY() / factor);
                }
            }
        }
    }

    private void moveTeethDefault(int zoom) {
        if (zoom < 0) {
            for (Node aux : teethPane.getChildren()) {
                for (int i = zoom; i < 0; i++) {
                    ((ImageView) (aux)).setX((((ImageView) aux).getX() * factor));
                    ((ImageView) (aux)).setY((((ImageView) aux).getY() * factor));
                    ((ImageView) (aux)).setScaleX(((ImageView) (aux)).getScaleX() * factor);
                    ((ImageView) (aux)).setScaleY(((ImageView) (aux)).getScaleY() * factor);
                }
            }
        } else if (zoom > 0) {
            for (Node aux : teethPane.getChildren()) {
                for (int i = zoom; i > 0; i--) {
                    ((ImageView) (aux)).setX((((ImageView) aux).getX() / factor));
                    ((ImageView) (aux)).setY((((ImageView) aux).getY() / factor));
                    ((ImageView) (aux)).setScaleX(((ImageView) (aux)).getScaleX() / factor);
                    ((ImageView) (aux)).setScaleY(((ImageView) (aux)).getScaleY() / factor);
                }
            }
        }
    }

    //TOOLS
    @FXML
    public void addMolarDown(MouseEvent event) {

        if ((event.getButton() == MouseButton.PRIMARY) && (addMolarDown != null && addMolarDown.isSelected())) {

            
            File f = new File("src\\main\\resources\\com\\group3\\dentalappmaven\\images\\paint\\molarDown.png");
            BufferedImage bufferedImage;

            try {

                bufferedImage = ImageIO.read(f);
                Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                ImageView aux = new ImageView(image);
                aux.setId("MolarDown.png");
                
                aux.setCursor(Cursor.MOVE);
                aux.setOnMousePressed(imageOnMousePressedEventHandler);
                aux.setOnMouseDragged(imageOnMouseDraggedEventHandler);
                aux.setOnMouseClicked(imageOnMouseRightClickEventHandler);
                aux.setX(event.getX() - aux.getImage().getWidth() / 2);
                aux.setY(event.getY() - aux.getImage().getHeight() / 2);
                System.out.println("Event X: " + aux.getX());
                System.out.println("Event Y: " + aux.getY());
                rescale(aux);
                teethPane.getChildren().add(aux);
                addMolarDown.setSelected(false);

            } catch (IOException ex) {
                System.out.println(ex.getMessage() + " " + f.getAbsolutePath());
            }
        }
    }

    @FXML
    public void addMolarUp(MouseEvent event) {

        if ((event.getButton() == MouseButton.PRIMARY) && (addMolarUp != null && addMolarUp.isSelected())) {

            
            File f = new File("src\\main\\resources\\com\\group3\\dentalappmaven\\images\\paint\\molarUp.png");
            BufferedImage bufferedImage;

            try {

                bufferedImage = ImageIO.read(f);
                Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                ImageView aux = new ImageView(image);
                aux.setId("molarUp.png");
                
                aux.setCursor(Cursor.MOVE);
                aux.setOnMousePressed(imageOnMousePressedEventHandler);
                aux.setOnMouseDragged(imageOnMouseDraggedEventHandler);
                aux.setOnMouseClicked(imageOnMouseRightClickEventHandler);

                aux.setX(event.getX() - aux.getImage().getWidth() / 2);
                aux.setY(event.getY() - aux.getImage().getHeight() / 2);

                rescale(aux);
                teethPane.getChildren().add(aux);
                addMolarUp.setSelected(false);

            } catch (IOException ex) {
                System.out.println(ex.getMessage() + " " + f.getAbsolutePath());
            }
        }
    }

    @FXML
    public void addIncisorDown(MouseEvent event) {

        if ((event.getButton() == MouseButton.PRIMARY) && (addIncisorDown != null && addIncisorDown.isSelected())) {

            
            File f = new File("src\\main\\resources\\com\\group3\\dentalappmaven\\images\\paint\\insisorDown.png");
            BufferedImage bufferedImage;

            try {

                bufferedImage = ImageIO.read(f);
                Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                ImageView aux = new ImageView(image);
                aux.setId("insisorDown.png");
                
                aux.setCursor(Cursor.MOVE);
                aux.setOnMousePressed(imageOnMousePressedEventHandler);
                aux.setOnMouseDragged(imageOnMouseDraggedEventHandler);
                aux.setOnMouseClicked(imageOnMouseRightClickEventHandler);

                aux.setX(event.getX() - aux.getImage().getWidth() / 2);
                aux.setY(event.getY() - aux.getImage().getHeight() / 2);

                rescale(aux);
                teethPane.getChildren().add(aux);
                addIncisorDown.setSelected(false);

            } catch (IOException ex) {
                System.out.println(ex.getMessage() + " " + f.getAbsolutePath());
            }
        }
    }

    @FXML
    public void addIncisorUp(MouseEvent event) {

        if ((event.getButton() == MouseButton.PRIMARY) && (addIncisorUp != null && addIncisorUp.isSelected())) {

            
            File f = new File("src\\main\\resources\\com\\group3\\dentalappmaven\\images\\paint\\insisorUp.png");
            BufferedImage bufferedImage;

            try {

                bufferedImage = ImageIO.read(f);
                Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                ImageView aux = new ImageView(image);
                aux.setId("insisorUp.png");
                
                aux.setCursor(Cursor.MOVE);
                aux.setOnMousePressed(imageOnMousePressedEventHandler);
                aux.setOnMouseDragged(imageOnMouseDraggedEventHandler);
                aux.setOnMouseClicked(imageOnMouseRightClickEventHandler);

                aux.setX(event.getX() - aux.getImage().getWidth() / 2);
                aux.setY(event.getY() - aux.getImage().getHeight() / 2);

                rescale(aux);
                teethPane.getChildren().add(aux);
                addIncisorUp.setSelected(false);

            } catch (IOException ex) {
                System.out.println(ex.getMessage() + " " + f.getAbsolutePath());
            }
        }
    }

    @FXML
    public void addPoint(MouseEvent event) {

        if ((event.getButton() == MouseButton.PRIMARY) && (addMolarDown != null && addMolarDown.isSelected())) {
            addMolarDown(event);
        }
        if ((event.getButton() == MouseButton.PRIMARY) && (addMolarUp != null && addMolarUp.isSelected())) {
            addMolarUp(event);
        }
        if ((event.getButton() == MouseButton.PRIMARY) && (addIncisorDown != null && addIncisorDown.isSelected())) {
            addIncisorDown(event);
        }
        if ((event.getButton() == MouseButton.PRIMARY) && (addIncisorUp != null && addIncisorUp.isSelected())) {
            addIncisorUp(event);
        }
        if ((event.getButton() == MouseButton.PRIMARY) && ((addPointBt != null && addPointBt.isSelected()) || (createOutlineBt != null && createOutlineBt.isSelected()))) {
            Circle aux = new Circle(3);
            aux.setCursor(Cursor.MOVE);
            aux.setCenterX(event.getX());
            aux.setCenterY(event.getY());
            aux.setOnMouseClicked(circleOnMouseRightClickEventHandler);
            aux.setOnMousePressed(circleOnMousePressedEventHandler);

            if (addPointBt.isSelected()) {
                aux.setFill(Color.YELLOW);
                aux.setOnMouseDragged(circleOnMouseDraggedEventHandler);
                System.out.println(event.getX() + ":" + event.getY());
                pointPane.getChildren().add(aux);
            } else if (createOutlineBt.isSelected()) {
                
                aux.setRadius(5);
                aux.setFill(Color.DEEPPINK.deriveColor(1, 1, 1, 0.5));
                aux.setStroke(Color.DEEPPINK);
                aux.setStrokeWidth(2);
                aux.setStrokeType(StrokeType.OUTSIDE);
                
                aux.setStrokeWidth(2);
                aux.setOnMouseDragged(circleOutlineOnMouseDraggedEventHandler);
                aux.setOnMouseClicked(circleOutlineOnMouseRightClickEventHandler);
                System.out.println(event.getX() + ":" + event.getY());
                outlinePane.getChildren().add(aux);
                int n = Integer.parseInt(nPointOutlineLb.getText());
                n--;
                nPointOutlineLb.setText("" + n);
                if (n == 0) {
                    toolsPane.setDisable(false);
                    createOutlineBt.setSelected(false);
                    createOutlineBt.setVisible(false);
                    rebuildOutlineBt.setVisible(true);
                    nPointOutlineLb.setTextFill(Color.GREEN);
                    createOutline();
                }
            }
        }
    }

    @FXML
    public void addPointOutline() throws IOException {
        DisableMeasuresOptionsToElements();
        toolsPane.setDisable(true);
        if (first) {
            openOutlineTutorial();
            first = false;
        }
    }

    //EVENTS
    EventHandler<MouseEvent> circleOnMousePressedEventHandler = new EventHandler<MouseEvent>() {

        @Override
        public void handle(MouseEvent t) {
            orgSceneX = t.getSceneX();
            orgSceneY = t.getSceneY();
            orgTranslateX = ((Circle) (t.getSource())).getCenterX();
            orgTranslateY = ((Circle) (t.getSource())).getCenterY();
        }
    };

    EventHandler<MouseEvent> circleOnMouseDraggedEventHandler = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent t) {
            double offsetX = t.getSceneX() - orgSceneX;
            double offsetY = t.getSceneY() - orgSceneY;
            double newTranslateX = orgTranslateX + offsetX;
            double newTranslateY = orgTranslateY + offsetY;
            ((Circle) (t.getSource())).setCenterX(newTranslateX);
            ((Circle) (t.getSource())).setCenterY(newTranslateY);
        }
    };

    EventHandler<MouseEvent> circleOutlineOnMouseDraggedEventHandler = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent t) {
            double offsetX = t.getSceneX() - orgSceneX;
            double offsetY = t.getSceneY() - orgSceneY;
            double newTranslateX = orgTranslateX + offsetX;
            double newTranslateY = orgTranslateY + offsetY;
            ((Circle) (t.getSource())).setCenterX(newTranslateX);
            ((Circle) (t.getSource())).setCenterY(newTranslateY);
            
            ObservableList<Node> curvesOld = outlinePane.getChildren().filtered(p -> p.getClass().getName().equals(javafx.scene.shape.CubicCurve.class.getName()));
            
            ObservableList<Node> points = outlinePane.getChildren().filtered(p -> p.getClass().getName().equals(javafx.scene.shape.Circle.class.getName()));
            
            for (int i = 0; i < points.size() - 1; i++) {
                rebuildOutline(curvesOld, (Circle) points.get(i), (Circle) points.get(i + 1), i);
            }
        }
    };

    EventHandler<MouseEvent> circleOnMouseRightClickEventHandler = new EventHandler<MouseEvent>() {

        @Override
        public void handle(MouseEvent t) {
            if (t.getButton() == MouseButton.SECONDARY) {
                clearPoint((Circle) t.getSource());
            } else if ((distance.isSelected() || angle.isSelected()) && t.getButton() == MouseButton.PRIMARY) {
                Circle c = (Circle) t.getSource();
                mediciones.add(c);
                c.setFill(Color.CYAN);
                if (mediciones.size() == 2 && distance.isSelected()) {
                    distance();
                }
                if (mediciones.size() == 3 && angle.isSelected()) {
                    angle();
                }
            } else if (t.getButton() == MouseButton.MIDDLE) {
                nPointOutlineLb.setText("" + (outlinePane.getChildren().filtered(p -> p.getClass() == Circle.class).indexOf((Circle) t.getSource()) + 1));
            }
        }
    };

    private void restoreMeasures() {
        for (int i = 0; i < mediciones.size(); i++) {
            Circle c = (Circle) mediciones.get(i);
            c.setFill(Color.YELLOW);
        }
        mediciones = new ArrayList<Circle>();
    }

    private void distance() {
        Circle c1 = mediciones.get(0);
        Circle c2 = mediciones.get(1);

        double x = Math.pow(c2.getCenterX() - c1.getCenterX(), 2);
        double y = Math.pow(c2.getCenterY() - c1.getCenterY(), 2);
        double d = Math.sqrt(x + y);

        if (zoom < 0) {
            for (int i = zoom; i < 0; i++) {
                d *= (factor);
            }
        } else if (zoom > 0) {
            for (int i = zoom; i > 0; i--) {
                d /= (factor);
            }
        }
        
        int dpi = java.awt.Toolkit.getDefaultToolkit().getScreenResolution();
        double cm = d * 2.54 / dpi;
        DecimalFormat df = new DecimalFormat("#.###");
        restoreMeasures();
        distanceLb.setText(df.format(cm) + " cm");
        distance.setSelected(false);
        d = 0;
    }

    private void angle() {
        Circle c1 = mediciones.get(0);
        Circle c2 = mediciones.get(1); // Centro
        Circle c3 = mediciones.get(2);

        double x0 = c1.getCenterX() - c2.getCenterX();
        double y0 = c1.getCenterY() - c2.getCenterY();
        double x1 = c3.getCenterX() - c2.getCenterX();
        double y1 = c3.getCenterY() - c2.getCenterY();

        double ang_pi = Math.atan2(x0, y0);
        double ang_pj = Math.atan2(x1, y1);
        double ang = ang_pj - ang_pi;

        if (ang < 0.0) {
            ang = ang + (2.0 * Math.PI);
        }
        ang = Math.toDegrees(ang);
        restoreMeasures();
        if (ang > 180) {
            ang = 360 - ang;
        }
        DecimalFormat df = new DecimalFormat("###.###");
        angleLb.setText(df.format(ang) + "º");
        angle.setSelected(false);
    }

    EventHandler<MouseEvent> circleOutlineOnMouseRightClickEventHandler = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent t) {
            if (t.getButton() == MouseButton.SECONDARY) {
                clearOutline();
            } else if (t.getButton() == MouseButton.MIDDLE) {
                nPointOutlineLb.setText("" + (outlinePane.getChildren().filtered(p -> p.getClass() == Circle.class).indexOf((Circle) t.getSource()) + 1));
            }
        }
    };

    private void clearPoint(Circle circle) {
        pointPane.getChildren().remove(circle);
    }

    private void clearTooth(ImageView circle) {
        teethPane.getChildren().remove(circle);
    }

    private void clearOutline() {
        outlinePane.getChildren().clear();
        nPointOutlineLb.setText("" + nPoint);
        nPointOutlineLb.setTextFill(Color.RED);
        createOutlineBt.setVisible(true);
        rebuildOutlineBt.setVisible(false);
        first = true;
    }
    
    EventHandler<MouseEvent> imageOnMousePressedEventHandler
            = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent t) {
            orgSceneX = t.getSceneX();
            orgSceneY = t.getSceneY();
            orgTranslateX = ((ImageView) (t.getSource())).getX();
            orgTranslateY = ((ImageView) (t.getSource())).getY();
        }
    };

    EventHandler<MouseEvent> imageOnMouseDraggedEventHandler
            = new EventHandler<MouseEvent>() {

        @Override
        public void handle(MouseEvent t) {
            double offsetX = t.getSceneX() - orgSceneX;
            double offsetY = t.getSceneY() - orgSceneY;
            double newTranslateX = orgTranslateX + offsetX;
            double newTranslateY = orgTranslateY + offsetY;
            System.out.println("DRAGGABLE");
            ((ImageView) (t.getSource())).setX(newTranslateX);
            ((ImageView) (t.getSource())).setY(newTranslateY);

        }
    };

    EventHandler<MouseEvent> imageOnMouseRightClickEventHandler = new EventHandler<MouseEvent>() {

        @Override
        public void handle(MouseEvent t) {
            if (t.getButton() == MouseButton.PRIMARY) {
                if (selectedImg != null) {
                    restoreSelectedItem(selectedImg);
                }
                selectedImg = (ImageView) t.getSource();
                String name = normalToSelected(selectedImg.getId());
                changeSrcImg(selectedImg, name);
            } else if (t.getButton() == MouseButton.SECONDARY) {
                clearTooth((ImageView) t.getSource());
            } else if (t.getButton() == MouseButton.MIDDLE) {
                if (selectedImg != null) {
                    restoreSelectedItem(selectedImg);
                    selectedImg = null;
                }
            }
        }
    };

    @FXML
    public void rotateRight() {
        if (selectedImg != null) {
            selectedImg.setRotate(selectedImg.getRotate() + 1);
        }
    }

    @FXML
    public void rotateLeft() {
        if (selectedImg != null) {
            selectedImg.setRotate(selectedImg.getRotate() - 1);
        }
    }

    private void rescale(ImageView aux) {
        if (zoom < 0) {
            for (int i = 0; i < Math.abs(zoom); i++) {
                ((ImageView) (aux)).setScaleX(((ImageView) (aux)).getScaleX() / factor);
                ((ImageView) (aux)).setScaleY(((ImageView) (aux)).getScaleY() / factor);
            }
        } else if (zoom > 0) {
            for (int i = 0; i < zoom; i++) {
                ((ImageView) (aux)).setScaleX(((ImageView) (aux)).getScaleX() * factor);
                ((ImageView) (aux)).setScaleY(((ImageView) (aux)).getScaleY() * factor);
            }
        }
    }

    @FXML
    public void openAbout() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("FXMLabout.fxml"));
        Stage stage = new Stage();
        stage.setScene(new Scene(loader.load()));
        stage.setResizable(false);
        stage.setTitle("About...");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }

    public void openTutorial() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("FXMLhelp.fxml"));
        Stage stage = new Stage();
        stage.setScene(new Scene(loader.load()));
        stage.setResizable(false);
        stage.setTitle("Help");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }

    public void openOutlineTutorial() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("FXMLtutorialOutline.fxml"));
        Stage stage = new Stage();
        stage.setScene(new Scene(loader.load()));
        stage.setResizable(false);
        stage.setTitle("Tutorial");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }

    @FXML
    public void buildOutline() {
        outlinePane.getChildren().removeIf(p -> p.getClass() != Circle.class);
        createOutline();
    }

    private void createOutline() {
        ObservableList<Node> iPoint = outlinePane.getChildren();
        for (int i = 0; i < nPoint - 1; i++) {
            Circle c1 = (Circle) iPoint.get(i);
            Circle c2 = (Circle) iPoint.get(i + 1);
            addCubicCurve(c1, c2);
        }
    }

    private void addCubicCurve(Circle c1, Circle c2) {
        CubicCurveManipulator aux = new CubicCurveManipulator(c1, c2);
        outlinePane.getChildren().add(aux.getControlLine1());
        outlinePane.getChildren().add(aux.getControlLine2());
        outlinePane.getChildren().add(aux.getControl1());
        outlinePane.getChildren().add(aux.getControl2());
        outlinePane.getChildren().add(aux.getCurve());
    }

    private void rebuildOutline(ObservableList<Node> curvesOld, Circle c1, Circle c2, int i) {
        CubicCurve c = (CubicCurve) curvesOld.get(i);
        changePoint(c, c1.getCenterX(), c1.getCenterY(), c2.getCenterX(), c2.getCenterY());
    }

    @FXML
    public void DisableElementsOptionsToMeasures() {
        selectedImg = null;
        if (addPointBt.isSelected()) {
            addPointBt.setSelected(false);
        }
        if (createOutlineBt.isSelected()) {
            createOutlineBt.setSelected(false);
        }

        ToggleButton t = (ToggleButton) groupTeeth.getSelectedToggle();
        if (t != null) {
            t.setSelected(false);
        }

        restoreMeasures();
        rebuildOutlineBt.setDisable(false);
    }

    @FXML
    public void DisableMeasuresOptionsToElements() {
        selectedImg = null;
        if (distance.isSelected()) {
            distance.setSelected(false);
        }
        if (angle.isSelected()) {
            angle.setSelected(false);
        }

        ToggleButton t = (ToggleButton) groupTeeth.getSelectedToggle();
        if (t != null) {
            t.setSelected(false);
        }

    }

    @FXML
    public void DisableAllOptionsToAddTeeth() {

        if (distance.isSelected()) {
            distance.setSelected(false);
        }
        if (angle.isSelected()) {
            angle.setSelected(false);
        }
        if (addPointBt.isSelected()) {
            addPointBt.setSelected(false);
        }
    }

    private void changeSrcImg(ImageView img, String file) {

        File f = new File("src\\main\\resources\\com\\group3\\dentalappmaven\\images\\paint\\" + file);
        BufferedImage bufferedImage;

        try {

            bufferedImage = ImageIO.read(f);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            img.setImage(image);

        } catch (IOException ex) {
            System.out.println(ex.getMessage() + " " + f.getAbsolutePath());
        }

    }

    private String normalToSelected(String id) {
        String newid = "";
        switch (id) {
            case "MolarDown.png":
                newid = "MolarDownS.png";
                break;
            case "molarUp.png":
                newid = "molarUpS.png";
                break;
            case "insisorDown.png":
                newid = "insisorDownS.png";
                break;
            case "insisorUp.png":
                newid = "insisorUpS.png";
                break;
        }
        return newid;
    }

    public void restoreSelectedItem(ImageView selected) {
        if (selected != null) {
            changeSrcImg(selected, selected.getId());
        }
    }
}
