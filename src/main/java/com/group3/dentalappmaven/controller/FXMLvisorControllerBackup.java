/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group3.dentalappmaven.controller;

import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;

/**
 * FXML Controller class
 *
 * @author alumno
 */
public class FXMLvisorControllerBackup implements Initializable {

    /**
     * Initializes the controller class.
     */
    //MENU
    @FXML
    MenuItem closeFileMenu;
    @FXML
    MenuItem openFileMenu;
    @FXML
    MenuItem saveAsMenu;

    //PANELS
    @FXML
    AnchorPane ap;
    @FXML
    ImageView visor;
    @FXML
    ImageView visorNoImage;
    @FXML
    Pane paintPane;
    @FXML
    Button open;
    @FXML
    StackPane viewerPane;
    @FXML
    ScrollPane s1;
    @FXML
    ToggleButton addPointBt;
    //MESSAGE
    @FXML
    Label lbStatus;

    double imagev; //VERTICAL HEIGHT
    double imageh; //HORIZONTAL WIDTH
    double defaulth; //DEFAULT ZOOM H
    double defaultv; //DEFAULT ZOOM W
    double orgSceneX, orgSceneY;
    double orgTranslateX, orgTranslateY;
    //ResizableCanvas paintZone;
    Group points=new Group();
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        File f = new File("src\\main\\resources\\com\\group3\\dentalappmaven\\images\\noimage.png");
        BufferedImage bufferedImage;
        try {
            bufferedImage = ImageIO.read(f);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            visorNoImage.setImage(image);
            visorNoImage.setDisable(true);

        } catch (IOException ex) {
            System.out.println(ex.getMessage() + " " + f.getAbsolutePath());
        }
        lbStatus.setText("Open File...");
        /*paintZone = new ResizableCanvas();
        //paintZone.setOnMouseClicked(value);
        paintZone.widthProperty().bind(
                visor.fitWidthProperty());
        paintZone.heightProperty().bind(
                visor.fitHeightProperty());
        viewerPane.getChildren().add(paintZone);*/
        visor.setFitHeight(s1.getHeight());
        visor.setFitWidth(s1.getWidth());
        visor.setVisible(false);
        s1.setStyle("-fx-font-size: 25px;");
        //viewerPane.getChildren().addAll(points);
        //paintZone.setVisible(false);
        //System.out.println(addPointBt.isSelected());
    }

    @FXML
    public void openPicture(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Picture File");
        FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("Pictures files (*.jpg, *.png)", "*.JPG","*.PNG");
        fileChooser.getExtensionFilters().addAll(extFilterJPG);
        File f = fileChooser.showOpenDialog(null);
        try {
            BufferedImage bufferedImage = ImageIO.read(f);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            //paintZone.setImage(image);
            visor.setPreserveRatio(true);
            visor.setImage(image);
            imagev=image.getHeight();
            //imageh=image.getWidth();
            visor.setFitHeight(imagev);
            visor.setFitWidth(imageh);
            defaulth = imageh;
            defaultv = imagev;
            lbStatus.setText("File " + f.getName());
            visorNoImage.setVisible(false);
            //paintZone.setVisible(true);
            visor.setVisible(true);
            Stage stage = (Stage) ap.getScene().getWindow();
            stage.setTitle("DentalApp - " + f.getName());
            openFileMenu.setDisable(true);
            closeFileMenu.setDisable(false);
            saveAsMenu.setDisable(false);
        }catch (IOException ex){
            lbStatus.setText("Error open file " + f.getName());
        }
    }

    @FXML
    public void closePicture(ActionEvent event) {
        visor.setVisible(false);
        //paintZone.setVisible(false);
        
        visorNoImage.setVisible(true);
        openFileMenu.setDisable(false);
        closeFileMenu.setDisable(true);
        saveAsMenu.setDisable(true);
        //paintZone.clear();
    }

    @FXML
    public void saveAs(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();

        //Set extension filter
        FileChooser.ExtensionFilter extFilter
                = new FileChooser.ExtensionFilter("png files (*.png)", "*.png");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File file = fileChooser.showSaveDialog(null);

        if (file != null) {
            try {
                WritableImage writableImage = new WritableImage((int) visor.getFitWidth(), (int) visor.getFitHeight());
                //paintZone.snapshot(null, writableImage);
                RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);
                ImageIO.write(renderedImage, "png", file);
            } catch (IOException ex) {
                lbStatus.setText("ERROR: Can't save file");
            }
        }
    }
    
    @FXML
    public void closeWindow(ActionEvent event){
        Platform.exit();
    }

    @FXML
    public void zoomPlus() {
        imagev = imagev * 2;
        imageh = imageh * 2;
        visor.setFitWidth(imageh);
        visor.setFitHeight(imagev);
        System.out.println("VISOR: " + visor.getFitWidth() + ":" + visor.getFitHeight());
//        System.out.println("CANVAS: " + paintZone.getWidth() + ":" + paintZone.getHeight());
    }

    @FXML
    public void zoomMinus() {
        imagev = imagev / 2;
        imageh = imageh / 2;
        visor.setFitWidth(imageh);
        visor.setFitHeight(imagev);
        System.out.println("VISOR: " + visor.getFitWidth() + ":" + visor.getFitHeight());
  //      System.out.println("CANVAS: " + paintZone.getWidth() + ":" + paintZone.getHeight());
    }

    @FXML
    public void zoomDefault() {
        visor.setFitWidth(defaulth);
        visor.setFitHeight(defaultv);
        imagev = defaultv;
        imageh = defaulth;
        System.out.println("VISOR: " + visor.getFitWidth() + ":" + visor.getFitHeight());
      //  System.out.println("CANVAS: " + paintZone.getWidth() + ":" + paintZone.getHeight());
    }

    @FXML
    public void doZoom(MouseEvent event) {
        if (event.getButton() == MouseButton.SECONDARY) {
            zoomMinus();
        } else if (event.getButton() == MouseButton.PRIMARY) {
            zoomPlus();
        } else if (event.getButton() == MouseButton.MIDDLE) {
            zoomDefault();
        }
    }

    @FXML
    public void addTeethDown() {
       // GraphicsContext graphicsContext = paintZone.getGraphicsContext2D();
        File f = new File("src\\main\\resources\\com\\group3\\dentalappmaven\\images\\paint\\muelainferior.png");
        BufferedImage bufferedImage;
        try {
            bufferedImage = ImageIO.read(f);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
        //    graphicsContext.drawImage(image, 50, 50, 48, 64);
        } catch (IOException ex) {
            System.out.println(ex.getMessage() + " " + f.getAbsolutePath());
        }

    }
    
    @FXML
    public void addPoint(MouseEvent event){
        System.out.println(addPointBt.isSelected());
        if(addPointBt.isSelected()){
            Circle aux=new Circle(5,Color.RED);
            aux.setCursor(Cursor.MOVE);
            aux.setCenterX(event.getSceneX());
            aux.setCenterY(event.getSceneY());
            aux.setOnMousePressed(circleOnMousePressedEventHandler);
            aux.setOnMouseDragged(circleOnMouseDraggedEventHandler);
            System.out.println(event.getX()+":"+event.getY());
            viewerPane.getChildren().add(aux);
            //paintPane.getChildren().add(aux);
            //points.getChildren().add(aux);
        }
        
        //System.out.println(addPointBt.isSelected());
        
    }
    
    EventHandler<MouseEvent> circleOnMousePressedEventHandler = 
        new EventHandler<MouseEvent>() {
 
        @Override
        public void handle(MouseEvent t) {
            orgSceneX = t.getSceneX();
            orgSceneY = t.getSceneY();
            orgTranslateX = ((Circle)(t.getSource())).getTranslateX();
            orgTranslateY = ((Circle)(t.getSource())).getTranslateY();
        }
    };
     
    EventHandler<MouseEvent> circleOnMouseDraggedEventHandler = 
        new EventHandler<MouseEvent>() {
 
        @Override
        public void handle(MouseEvent t) {
            double offsetX = t.getSceneX() - orgSceneX;
            double offsetY = t.getSceneY() - orgSceneY;
            double newTranslateX = orgTranslateX + offsetX;
            double newTranslateY = orgTranslateY + offsetY;
            System.out.println("DRAGGABLE");
            ((Circle)(t.getSource())).setTranslateX(newTranslateX);
            ((Circle)(t.getSource())).setTranslateY(newTranslateY);
        }
    };
    
}
