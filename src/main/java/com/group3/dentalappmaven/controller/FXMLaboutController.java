/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group3.dentalappmaven.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author alumno
 */
public class FXMLaboutController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    @FXML
    Label creditLb;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        creditLb.setText("Creditos a:\n\t"
                +"- Mateo Werpulewski\n\t"
                +"- Edgar Pérez Ferrando\n\t"
                +"- Jose Miguel Torres Maldonado");
    }    
    
}
