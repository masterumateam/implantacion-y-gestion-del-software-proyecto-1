/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.group3.dentalappmaven.controller;

/**
 *
 * @author Jose
 */
import javafx.beans.property.DoubleProperty;
import javafx.event.EventHandler;
import javafx.scene.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;

/**
 * Example of how a cubic curve works, drag the anchors around to change the
 * curve.
 */
public class CubicCurveManipulator {

    Line controlLine1;
    Line controlLine2;
    Anchor control1;
    Anchor control2;
    CubicCurve curve;

    /**
     *
     * @param start
     * @param end
     */
    public CubicCurveManipulator(Circle start, Circle end) {

        curve = new CubicCurve();
        curve.setStartX(start.getCenterX());
        curve.setStartY(start.getCenterY());
        curve.setControlX1(start.getCenterX() - 35);
        curve.setControlY1(start.getCenterY() + 35);
        curve.setControlX2(end.getCenterX() + 35);
        curve.setControlY2(end.getCenterY() - 35);
        curve.setEndX(end.getCenterX());
        curve.setEndY(end.getCenterY());
        curve.setStroke(Color.FORESTGREEN);
        curve.setStrokeWidth(2);
        curve.setStrokeLineCap(StrokeLineCap.ROUND);
        curve.setFill(Color.TRANSPARENT);
        controlLine1 = new BoundLine(curve.controlX1Property(), curve.controlY1Property(), curve.startXProperty(), curve.startYProperty());
        controlLine2 = new BoundLine(curve.controlX2Property(), curve.controlY2Property(), curve.endXProperty(), curve.endYProperty());
        control1 = new Anchor(Color.ORANGE, curve.controlX1Property(), curve.controlY1Property());
        control2 = new Anchor(Color.RED, curve.controlX2Property(), curve.controlY2Property());
    }

    public CubicCurveManipulator(Circle start, Circle end, double CX1, double CY1, double CX2, double CY2) {

        curve = new CubicCurve();
        curve.setStartX(start.getCenterX());
        curve.setStartY(start.getCenterY());
        curve.setControlX1(CX1);
        curve.setControlY1(CY1);
        curve.setControlX2(CX2);
        curve.setControlY2(CY2);
        curve.setEndX(end.getCenterX());
        curve.setEndY(end.getCenterY());
        curve.setStroke(Color.FORESTGREEN);
        curve.setStrokeWidth(2);
        curve.setStrokeLineCap(StrokeLineCap.ROUND);
        curve.setFill(Color.TRANSPARENT);
        controlLine1 = new BoundLine(curve.controlX1Property(), curve.controlY1Property(), curve.startXProperty(), curve.startYProperty());
        controlLine2 = new BoundLine(curve.controlX2Property(), curve.controlY2Property(), curve.endXProperty(), curve.endYProperty());
        control1 = new Anchor(Color.ORANGE, curve.controlX1Property(), curve.controlY1Property());
        control2 = new Anchor(Color.RED, curve.controlX2Property(), curve.controlY2Property());
    }

    class BoundLine extends Line {

        BoundLine(DoubleProperty startX, DoubleProperty startY, DoubleProperty endX, DoubleProperty endY) {
            startXProperty().bind(startX);
            startYProperty().bind(startY);
            endXProperty().bind(endX);
            endYProperty().bind(endY);
            setStrokeWidth(2);
            setStroke(Color.GRAY.deriveColor(0, 1, 1, 0.5));
            setStrokeLineCap(StrokeLineCap.BUTT);
            getStrokeDashArray().setAll(10.0, 5.0);
        }
    }

    // a draggable anchor displayed around a point.
    class Anchor extends Circle {

        Anchor(Color color, DoubleProperty x, DoubleProperty y) {
            super(x.get(), y.get(), 4);
            setFill(color.deriveColor(1, 1, 1, 0.5));
            setStroke(color);
            setStrokeWidth(2);
            setStrokeType(StrokeType.OUTSIDE);

            x.bind(centerXProperty());
            y.bind(centerYProperty());
            enableDrag();
        }

        // make a node movable by dragging it around with the mouse.
        private void enableDrag() {
            final Delta dragDelta = new Delta();
            setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    // record a delta distance for the drag and drop operation.
                    dragDelta.x = getCenterX() - mouseEvent.getX();
                    dragDelta.y = getCenterY() - mouseEvent.getY();
                    getScene().setCursor(Cursor.MOVE);
                }
            });
            setOnMouseReleased(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    getScene().setCursor(Cursor.HAND);
                }
            });
            setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    double newX = mouseEvent.getX() + dragDelta.x;
                    if (newX > 0 && newX < getScene().getWidth()) {
                        setCenterX(newX);
                    }
                    double newY = mouseEvent.getY() + dragDelta.y;
                    if (newY > 0 && newY < getScene().getHeight()) {
                        setCenterY(newY);
                    }
                }
            });
            setOnMouseEntered(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    if (!mouseEvent.isPrimaryButtonDown()) {
                        getScene().setCursor(Cursor.HAND);
                    }
                }
            });
            setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    if (!mouseEvent.isPrimaryButtonDown()) {
                        getScene().setCursor(Cursor.DEFAULT);
                    }
                }
            });
        }

        // records relative x and y co-ordinates.
        private class Delta {

            double x, y;
        }
    }

    public Line getControlLine1() {
        return controlLine1;
    }

    public Line getControlLine2() {
        return controlLine2;
    }

    /*public Anchor getStart() {
        return start;
    }*/
    public Anchor getControl1() {
        return control1;
    }

    public Anchor getControl2() {
        return control2;
    }

    /*public Anchor getEnd() {
        return end;
    }*/
    public CubicCurve getCurve() {
        return curve;
    }

    public void changeControl(double CX1, double CY1, double CX2, double CY2) {
        curve.setControlX1(CX1);
        curve.setControlY1(CY1);
        curve.setControlX2(CX2);
        curve.setControlY2(CY2);
    }

    public static void changePoint(CubicCurve curve,double SX, double SY, double EX, double EY) {
        /*curve.setControlX1(CX1);
        curve.setControlY1(CY1);
        curve.setControlX2(CX2);
        curve.setControlY2(CY2);*/
        curve.setStartX(SX);
        curve.setStartY(SY);
        curve.setEndX(EX);
        curve.setEndY(EY);
    }
}
